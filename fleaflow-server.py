#!/usr/bin/python

# author : Sven Wagner http://www.nerdquadrat.de
# license: GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html


import sys
import os
sys.path.append("lib")
from flask import Flask, request, Response, jsonify
import fleaflow
import fleaflow_server
import flealock
import yaml
import time
import logging
import logging.handlers
import threading
import time
import datetime
import urllib


# TODO on the fly switch
DEBUG = False
DEBUG = True


log = logging.getLogger('myapp')
hdlr = logging.handlers.WatchedFileHandler('log/fleaflow-server.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
log.addHandler(hdlr) 
log.setLevel(logging.DEBUG)
log.info("INIT [%i] with args %s" % (os.getpid(), sys.argv))

app = Flask(__name__)

SREPOS = {}

# configurations that are not reloaded on the fly
CONFIG_FILE = "repos.yaml"
PORT = 5000
LISTEN = "0.0.0.0"

# constants
COMMIT_LENGTH = 40

SPIDER = 0
INFO = 1
ARCHIVE = 2
FILES = 3

class dummy_class():
    pass

conf = dummy_class()
conf.modtime = 0

def reConf(SREPOS, conf, TID):
    log.info("[%s] reConf started" %  TID)
    del_list = []
    for repo in SREPOS:
        if repo in conf.repos:
            SREPOS[repo].reReadConfig()
        else:
            SREPOS[repo].stopCleanupManager()
            del_list.append(repo)
    for r in del_list:
        del SREPOS[r]
    for repo in conf.repos:
        if repo not in SREPOS:
            SREPOS[repo] = fleaflow_server.serveRepo(repo, conf)
    log.info("[%s] reConf ended" % TID)

def updateConfig(SREPOS, conf, TID):
    modtime = int(os.stat(CONFIG_FILE).st_mtime)
    if modtime == conf.modtime:
        log.info("[%s] updateConfig found same modtime" % TID)
        return
    log.info("[%s] updateConfig found new modtime" % TID)
    flo = flealock.flealock(CONFIG_FILE)
    try:
        if flo.lock(wait=-1):
            if modtime > conf.modtime:
                with open(CONFIG_FILE,"r") as fh:
                    data = yaml.load(fh.read())
                    conf.repos = data["repos"]
                    conf.TRUSTED_XFF_PROXIES = data["TRUSTED_XFF_PROXIES"]
                    conf.CLEANUP_SLEEP = data["CLEANUP_SLEEP"]
                conf.modtime = modtime
                conf.updated = True
    finally:
        if conf.updated:
            reConf(SREPOS, conf, TID)
        conf.updated = False
        flo.unlock()
    log.info("[%s] updateConfig ended" % TID)

def check_userdata(TID, single_files=False):
    repo = request.args.get("repo","")
    commit = request.args.get("commit","")
    if fleaflow_server.RE_REPO.match(repo) and fleaflow_server.RE_COMMIT.match(commit) and len(commit) == COMMIT_LENGTH:
        log.debug("[%s] check_userdata got %s %s" % (TID, repo, commit))
        if single_files:
            try:
                return (repo, commit, urllib.unquote(request.args.get("filelist","")).split(","))
            except:
                return False
        return (repo, commit)
    log.debug("[%s] check_userdata not successful for %s %s" % (TID, repo, commit))
    return False

def check_repo(repo, commit, client_ip, request_type, TID, filelist=None):
    if repo not in SREPOS:
        log.debug("[%s] check_repo cannot find repo %s" % (TID, repo))
        return "None, cannot find repo.\n", 404

    if not SREPOS[repo].check_acl(client_ip):
        log.debug("[%s] client_ip %s rejected for repo %s" % (TID, client_ip, repo))
        return "Sorry, it's not for you, it's for bidden.\n", 403

    result = SREPOS[repo].get_commit_info(commit, log, TID)

    if result is None:
        log.debug("[%s] cannot find commit %s in repo %s" % (TID, commit, repo))
        return "Nope, cannot find commit.\n", 404
    else:
        log.debug("[%s] commit %s found in repo %s" % (TID, commit, repo))
        if request_type == INFO:
            return jsonify(repo=repo, commit=commit, md5=result[0], bytes=result[1])
        elif request_type == SPIDER:
            return SREPOS[repo].spider_commit(commit, log, TID)
        elif request_type == ARCHIVE:
            return Response(SREPOS[repo].stream_tar(commit))
        elif request_type == FILES:
            return Response(SREPOS[repo].stream_tar(commit, filelist))


def get_client_ip(conf, TID):
    for client_ip in reversed(request.access_route):
        if client_ip not in conf.TRUSTED_XFF_PROXIES:
            log.debug("[%s] connected by client %s" % (TID, client_ip))
            return client_ip
    return False

@app.route("/spider")
@app.route("/spider_archive_file")
def spider_archive_file():
    TID = threading.current_thread().getName().replace("Thread-", "")
    log.debug("[%s] spider run updateconfig" % TID)
    updateConfig(SREPOS, conf, TID)
    log.debug("[%s] spider run get_client_ip" % TID)
    client_ip = get_client_ip(conf, TID)
    if not client_ip:
        log.debug("[%s] spider got bad IP" % TID)
        return "ixenternal error.\n", 1338
    try:
        log.debug("[%s] spider run check_userdata" % TID)
        repo, commit = check_userdata(TID)
    except:
        return "None, check repo/commit.\n", 420
    log.debug("[%s] run check_repo" % TID)
    return check_repo(repo, commit, client_ip, SPIDER, TID)

@app.route("/info")
@app.route("/get_commit_info")
def get_commit_info():
    TID = threading.current_thread().getName().replace("Thread-", "")

    updateConfig(SREPOS, conf, TID)

    log.debug("[%s] info run get_client_ip" % TID)
    client_ip = get_client_ip(conf, TID)
    if not client_ip:
        log.debug("[%s] info got bad IP" % TID)
        return "ixenternal error.\n", 1338

    try:
        log.debug("[%s] info run check_userdata" % TID)
        repo, commit = check_userdata(TID)
    except:
        return "None, check repo/commit.\n", 420

    log.debug("[%s] info run check_repo" % TID)
    return check_repo(repo, commit, client_ip, INFO, TID)

@app.route("/files")
@app.route("/get_zipped_files")
def get_zipped_tar():
    TID = threading.current_thread().getName().replace("Thread-", "")

    updateConfig(SREPOS, conf, TID)

    client_ip = get_client_ip(conf, TID)
    if not client_ip:
        log.debug("[%s] archive got bad IP" % TID)
        return "ixenternal error.\n", 1338

    try: # TODO proper return + no try/except
        log.debug("[%s] archive run check_userdata" % TID)
        repo, commit, filelist = check_userdata(TID, single_files=True)
    except:
        return "None, check repo/commit.\n", 420

    log.debug("[%s] archive run check_repo" % TID)
    return check_repo(repo, commit, client_ip, FILES, TID, filelist=filelist)



@app.route("/archive")
@app.route("/get_zipped_archive")
def get_zipped_archive():
    TID = threading.current_thread().getName().replace("Thread-", "")

    updateConfig(SREPOS, conf, TID)

    client_ip = get_client_ip(conf, TID)
    if not client_ip:
        log.debug("[%s] archive got bad IP" % TID)
        return "ixenternal error.\n", 1338

    try: # TODO proper return + no try/except
        log.debug("[%s] archive run check_userdata" % TID)
        repo, commit = check_userdata(TID)
    except:
        return "None, check repo/commit.\n", 420

    log.debug("[%s] archive run check_repo" % TID)
    return check_repo(repo, commit, client_ip, ARCHIVE, TID)

if __name__ == "__main__":
    app.run(host = LISTEN, port=PORT, debug=DEBUG, threaded = True)

