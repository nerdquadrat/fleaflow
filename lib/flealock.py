#!/usr/bin/python

# author : Sven Wagner http://www.nerdquadrat.de
# license: GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

import os, stat, re, time
import errno, fcntl

DEFAULT_LOCKDIR = "/var/lock"
DEFAULT_LOCK = "%s/.global_lock_file" % DEFAULT_LOCKDIR

RE_LOCK = re.compile("[0-9]+: +FLOCK +(ADVISORY|MANDATORY) +(WRITE|READ) +([0-9]+) +([0-9a-fA-F]+):([0-9a-fA-F]+):([0-9]+) +0 +EOF")

NOLOCK = 0
SHARED = 1
EXCLUSIVE = 2

INFO = 0
ERROR = 1
CRITICAL = 2

if "main" in vars():
    DEFAULT_FILENAME = os.path.basename(main.__file__)
else:
    try:
        open(DEFAULT_LOCK, "a").close()
        DEFAULT_FILENAME = DEFAULT_LOCK
    except:
        DEFAULT_FILENAME = "%s/.lock/default" % os.environ['HOME']

class flealock(object):
    """
    use atomic kernel based advisory locks (see fcntl flock)
    does not always work reliably on NFS
    """
    def __init__(self, filename=DEFAULT_FILENAME, logger=None, lockdir=DEFAULT_LOCKDIR):
        if filename.startswith("/"):
            self.__lockfile = filename
        else:
            self.__lockfile = "%s/%s.lck" % (lockdir, filename)
        if not os.path.exists(filename):
            try:
                if not os.path.exists(os.path.dirname(filename)):
                    os.makedirs(os.path.dirname(filename))
                os.close(os.open(self.__lockfile, os.O_CREAT|os.O_EXCL|os.O_APPEND))
            except:
                pass # either a concurrently created file or missing permissions

        open(self.__lockfile, "a").close() # fail on missing permissions

        self.__logger = logger
        self.__fh = None
        self.__shared = None

    def checkDeletedLockfile(self):
        """
        raise exception on deleted lockfile to prevent multiple locks for the same filename
        """
        if os.fstat(self.__fh.fileno()).st_nlink == 0:
            raise ValueError("bah! someone unlinked my lockfile: '%s' :(" % self.__lockfile)

    def log(self, text, level=INFO):
        if self.__logger in ["DEBUG", "VERBOSE"]:
            if level == DEBUG and self.__logger == "DEBUG":
                print("DEBUG: %s" % text)
            elif level == INFO:
                print("INFO: %s" % text)
            elif level == ERROR:
                print("ERROR: %s" % text)
            elif level == CRITICAL:
                print("CRITICAL: %s" % text)

        elif self.__logger:
            if level == INFO:
                self.__logger.info("INFO: %s" % text)
            elif level == ERROR:
                self.__logger.error("ERROR: %s" % text)
            elif level == CRITICAL:
                self.__logger.critical("CRITICAL: %s" % text)
        

    def getFilename(self):
        return self.__lockfile

    def __open_lock(self, opts):
        if self.__fh:
            fh = self.__fh
            close_after_try = False
        else:
            fh = open(self.__lockfile, "r+")
            close_after_try = True

        try:
            fcntl.flock(fh.fileno(), opts)
        except IOError, e:
            if e.errno != errno.EINTR:
                if close_after_try:
                    fh.close()
                raise e
            return False
        if fh:
            self.__fh = fh
            self.checkDeletedLockfile()
            return True
        return False

    def getSharedLock(self):
        return self.lock(shared=True)

    def getExclusiveLock(self):
        return self.lock(shared=False)

    def waitForSharedLock(self, wait=-1):
        return self.lock(shared=True, wait=wait)

    def waitForExclusiveLock(self, wait=-1):
        return self.lock(shared=False, wait=wait)

    def lock(self, wait=0, shared=False):
        """
        acquire lock, wait=-1 (eternal), wait=0 or wait=0.0 (nowait), wait= int|float>0 (wait timeout seconds)
        """
        if self.__fh and self.__shared == shared:
            return True
        opt = fcntl.LOCK_EX
        text = "EXCLUSIVE"
        if shared:
            text = "SHARED"
            opt = fcntl.LOCK_SH

        if self.__fh:
            self.log("trying to up/downgrade lock to %s for '%s' with timeout = %i" % (text, self.__lockfile, wait))
        else:
            self.log("trying to acquire %s lock for '%s' with timeout = %i" % (text, self.__lockfile, wait))
            
        if wait == -1:
            if self.__open_lock(opt):
                self.__shared = shared
                self.log("%s lock acquired for '%s'" % (text, self.__lockfile))
                return True
            self.log("FAILED to acquire %s lock for '%s'" % (text, self.__lockfile))
            return False
                
        if wait == 0:
            try:
                if self.__open_lock(opt | fcntl.LOCK_NB):
                    self.__shared = shared
                    self.log("%s lock acquired for '%s'" % (text, self.__lockfile))
                    return True
                self.log("FAILED to acquire %s lock for '%s'" % (text, self.__lockfile))
                return False
            except IOError, e:
                if e.errno in [errno.EACCES, errno.EAGAIN, errno.EINTR]:
                    self.log("FAILED to acquire %s lock for '%s'" % (text, self.__lockfile))
                    return False
                raise e
            

        if type(wait) in [int, float] and wait > 0:
            n = 0.0
            while n < wait:
                if self.__open_lock(opt | fcntl.LOCK_NB):
                    self.__shared = shared
                    self.log("%s lock acquired for '%s' within timeout (%f seconds left)" % (text, self.__lockfile, wait - n))
                    return True
                time.sleep(0.1)
                n += 0.1
            self.log("FAILED to acquire %s lock for '%s' within %f seconds timeout" % (text, self.__lockfile, float(wait)))
            return False

    def unlock(self):
        if self.isLocked():
            fcntl.flock(self.__fh.fileno(), fcntl.LOCK_UN)
            self.__fh.close()
            if self.__shared is True:
                text = "SHARED"
            elif self.__shared is False:
                text = "EXCLUSIVE"

            self.log("released %s lock from '%s'" % (text, self.__lockfile))
        else:
            self.log("FAILED to remove nonexisting lock from '%s'" % self.__lockfile)
            return False

        self.__fh = None
        self.__shared = None
        return True

    def release(self):
        return self.unlock()

    def isLocked(self):
        if self.__fh and self.__shared is not None:
            return True
        return False

    def isShared(self):
        return self.__shared

    def isExclusive(self):
        if self.__shared is None:
            return False
        return not self.__shared

    def getLockfileDeviceInode(self):
        if self.isLocked():
            stats = os.stat(self.__lockfile)
            self.checkDeletedLockfile()
            inode = stats[stat.ST_INO]
            device = stats[stat.ST_DEV]
            minor = os.minor(device)
            major = os.major(device)
            return (major, minor, inode)
        return None

    def spider(self, shared=False):
        """
        find out which locks are available or set
        """
        if self.isLocked():
            raise ValueError("spider not to be used on already acquired locks, create new object instead") # use: flealock(filename=x).spider(shared=False)
        if shared is None:
            if self.lock(shared=True):
                self.unlock()
                shared_available = True
            else:
                shared_available = False

            if self.lock(shared=False):
                self.unlock()
                exclusive_available = True
            else:
                exclusive_available = False

            if shared_available and exclusive_available:
                return NOLOCK
            if shared_available:
                return SHARED
            return EXCLUSIVE
        if self.lock(shared=shared):
            self.unlock()
            return True
        return False

    def getPidsWithLocks(self, with_my_locks = False):
        res = self.getLockfileDeviceInode()
        mykey = ":".join([str(hex(res[0]))[2:].zfill(2), str(hex(res[1]))[2:].zfill(2), str(res[2])])
        mypid = os.getpid()
        syslocks = []
        with open("/proc/locks") as fh:
            for line in fh:
                try:
                    am, rw, pid, major, minor, inode = RE_LOCK.findall(line)[0]
                except:
                    continue
                key = ":".join([major,minor,str(inode)])
                if key != mykey:
                    continue
                if int(pid) == mypid:
                    if with_my_locks is False:
                        continue
                    myself = True
                else:
                    myself = False

                syslocks.append({"am": am, "rw": rw, "pid": pid, "major": major, "minor": minor, "inode": inode, "its_me": myself})
        return syslocks

    def writeToLockfile(self, data, offset=0, ref=0):
        if self.isLocked() and not self.isShared():
            try:
                self.__fh.seek(offset, ref)
                self.__fh.write(data)
                self.__fh.truncate()
                return True
            except Exception as e:
                return e
        return False

    def appendToLockfile(self, data):
        return self.writeToLockfile(data, offset=0, ref=2)

    def readFromLockfile(self):
        if self.isLocked():
            self.__fh.seek(0, 0)
            return self.__fh.read()
        return False

    def getLockfileHandle(self):
        if self.isLocked():
            return self.__fh
        return None

if __name__== "__main__":
  main()

