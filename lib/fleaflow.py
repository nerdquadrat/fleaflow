#!/usr/bin/python

# author : Sven Wagner http://www.nerdquadrat.de
# license: GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

import sys, os, fnmatch, time
import threading
import tempfile
import flealock

# for flock timeouts
import errno, fcntl

DEFAULT_LOCKDIR = "/var/lock"
DEFAULT_LOCK = "%s/.global_flock_file" % DEFAULT_LOCKDIR

ST_MTIME = 8


def fs_walk(directory, filefilter):
    result = []
    for root, dirnames, filenames in os.walk(directory):
        for filename in fnmatch.filter(filenames, filefilter):
            result.append(os.path.join(root, filename))
    return result

class cleanupManager(object):
    def __init__(self, conf, lock, repo):
        self.__conf = conf
        self.__lock = lock
        self.__repo = repo

        self.__keeprunning = True
        self.__thread = threading.Thread(target=self.cleanup)
        self.__thread.setDaemon(True)
        self.__thread.start()

    def stopthread(self):
        self.__keeprunning = False
        self.__thread.join()

    def cleanup(self):
        while self.__keeprunning:
            conf = self.__conf.repos[self.__repo]
            now = int(time.time())
            for filename in fs_walk(conf["cache_dir"], '*.tar.gz'):
                if os.stat(filename)[ST_MTIME] < now - conf["cache_minutes"] * 60:
                    flo = flealock.flealock(filename)
                    if flo.lock(wait=False):
                        try:
                            os.unlink(filename)
                        finally:
                            flo.unlock()
                    flo.unlock()
                
            if conf["info_file"] is not None and os.path.exists(conf["info_file"]) and os.stat(conf["info_file"])[ST_MTIME] < now - conf["cache_minutes"] * 60:
                flo = flealock.flealock(conf["info_file"])
                if flo.lock(wait=2):
                    try:
                        with open(conf["info_file"], "r") as fh:
                            lines = fh.readlines()
                        tempf = tempfile.NamedTemporaryFile(delete=False, mode='w', dir=os.path.dirname(conf["info_file"]))
                        if len(lines) > conf["max_lines"] + conf["max_lines"] / 10:
                            for line in lines[len(lines)-conf["max_lines"]:]:
                                tempf.write(line)
                        tempf.close()
                        os.rename(tempf.name, conf["info_file"])
                        past = int(time.time()) - (conf["cache_minutes"] + 1) * 60
                        os.utime(conf["info_file"], (past, past))
#                        d = os.system("touch -d now-%iminutes '%s'" % (self.minutes + 1 , self.infofile))
                    finally:
                        flo.unlock()
            w = 0
            while w < self.__conf.CLEANUP_SLEEP and self.__keeprunning:
                w += 1
                time.sleep(1)

if __name__== "__main__":
  main()

