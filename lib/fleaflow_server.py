#!/usr/bin/python

# author : Sven Wagner http://www.nerdquadrat.de
# license: GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

from flask import Flask, request, Response
import sys, os, re, subprocess
sys.path.append("lib")
import fleaflow
import flealock
import yaml
import ipaddr
import threading

# remove
import time

RE_REPO = re.compile("^[a-zA-Z0-9-_]+$")
RE_COMMIT = re.compile("^[a-fA-F0-9]+$")
RE_MD5 = RE_COMMIT

class serveRepo(object):
    def __init__(self, name, conf, log=None):
        self.conf = conf
        self.name = name
        self.lock = threading.Lock()
        self.reReadConfig()
        self.log = log

    def reReadConfig(self):
        self.commit_info = {}
        self.allowed_nets = [ipaddr.IPv4Network(v) for v in self.conf.repos[self.name]["allowed_nets"]]
        if self.conf.repos[self.name]["type"] == "workdir":
            self.full_path = "%s/.git" % self.conf.repos[self.name]["path"]
        else:
            self.full_path = self.conf.repos[self.name]["path"]

        if not os.path.exists(self.conf.repos[self.name]["cache_dir"]):
            os.makedirs(self.conf.repos[self.name]["cache_dir"])
        try:
            self.stopCleanupManager()
        except:
            pass
        self.cleanupManager = fleaflow.cleanupManager(self.conf, self.lock, self.name)

    def stopCleanupManager(self):
        self.cleanupManager.stopthread()

    def check_acl(self,client_ip):
        try:
            ip = ipaddr.IPv4Network("%s/32" % client_ip)
            for net in self.allowed_nets:
                if ip.overlaps(net):
                    return True
        except:
            return False
        return False

    def spider_commit(self, commit, log, TID):
        cache_file = "%s/%s.tar.gz" % ( self.conf.repos[self.name]["cache_dir"], commit )
        if os.path.exists(cache_file):
            os.utime(cache_file, None)
            return "we have it\n", 200
        return "we don't\n", 404

    def get_commit_info(self, commit, log, TID):
        if commit in self.commit_info:
            cache_file = "%s/%s.tar.gz" % ( self.conf.repos[self.name]["cache_dir"], commit )
            if os.path.exists(cache_file):
                os.utime(cache_file, None)
            return self.commit_info[commit]
        if os.path.exists(self.conf.repos[self.name]["info_file"]):
            flo = flealock.flealock(self.conf.repos[self.name]["info_file"])
            try:
                if flo.lock(shared=True, wait=-1):
                    with open(self.conf.repos[self.name]["info_file"], "r") as fh:
                        for line in fh:
                            if line.startswith("#") or len(line) < 75:
                                continue
                            if line.startswith("%s " % commit):
                                try:
                                    commit, md5, size, branches = line.split()
                                except:
                                    continue
                                if self.conf.repos[self.name]["branches"] != "ALL":
                                    matchbranch = False
                                    if "," in branches:
                                        branches = branches.split(",")
                                    else:
                                        branches = [branches]
#                                    os.system("echo '%s' >> /tmp/foo" % str(branches))
                                    for b in self.conf.repos[self.name]["branches"]:
                                        if b in branches:
                                            matchbranch = True
                                            break
                                    if matchbranch is False:
                                        continue

                                self.commit_info[commit] = (md5, int(size))
                                flo.unlock()
                                cache_file = "%s/%s.tar.gz" % ( self.conf.repos[self.name]["cache_dir"], commit )
                                if os.path.exists(cache_file):
                                    os.utime(cache_file, None)
                                return self.commit_info[commit]
            finally:
                flo.unlock()
        else:
            open(self.conf.repos[self.name]["info_file"], "a").close()
        result = self.create_commit_info(commit)
        if result is None:
            return None
        self.commit_info[commit] = result
        flo = flealock.flealock(self.conf.repos[self.name]["info_file"])
        try:
            if flo.lock(shared=False, wait=-1):
                with open(self.conf.repos[self.name]["info_file"], "a") as fh:
                    fh.write("%s %s %s %s\n" % (commit, result[0], result[1], result[2]))
        finally:
            flo.unlock()
        return result
            
    def create_commit_info(self, commit):
        self.lock.acquire()
        if self.conf.repos[self.name]["branches"] != "ALL":
            # check if commit exists and in what branch it is
            cmd_getbranch = ["/usr/bin/git", "--git-dir=%s" % self.full_path, "branch", "--contains", commit]
            proc_getbranch = subprocess.Popen(cmd_getbranch, stdout=subprocess.PIPE, shell=False)
            proc_getbranch.wait()
            output, err = proc_getbranch.communicate()
            branches = [b.replace("*", "").strip() for b in output.splitlines()]
            if len(branches) == 0:
                return None

            matchbranch = False
            for b in self.conf.repos[self.name]["branches"]:
                if b in branches:
                    matchbranch = True
            if matchbranch is False:
                # TODO log access to not configured branch
                self.lock.release()
                return None
        else:
            branches = ["^^"]

        cache_file = "%s/%s.tar.gz" % ( self.conf.repos[self.name]["cache_dir"], commit )
        flo = flealock.flealock(cache_file)
        try:
            if flo.lock(wait=-1):
            # when having the lock, re-check info in info file, maybe its there now
                cmd_git = ["/usr/bin/git", "--git-dir=%s" % self.full_path, "archive", "--format", "tar", commit]
                proc_git = subprocess.Popen(cmd_git, stdout=subprocess.PIPE, shell=False)

                cmd_info = "/bin/bash -c '/usr/bin/tee >(gzip -c > %s) >(/usr/bin/md5sum 1>&2) | wc -c'" % cache_file
                proc_info = subprocess.Popen(cmd_info, stdin=proc_git.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
                proc_git.wait()
                proc_info.wait()
                output, err = proc_info.communicate()

                self.lock.release()
                flo.unlock()
                if proc_git.returncode == 0 and proc_info.returncode == 0 and RE_MD5.match(err[:32]):
                    return (err[:32], int(output.rstrip("\n")), ",".join(branches))
                return None
        finally:
            flo.unlock()
            if self.lock.locked():
                self.lock.release()

        return None

    def stream_tar(self, commit, filelist=None):
        self.conf.repos[self.name]["cache_dir"]
        cache_file = "%s/%s.tar.gz" % ( self.conf.repos[self.name]["cache_dir"], commit )
        if not filelist and os.path.exists(cache_file):
            flo = flealock.flealock(cache_file)
            try:
                if flo.lock(wait=-1, shared=True):
                    # d = os.system("touch '%s'" % cache_file)
                    os.utime(cache_file, None)
                    # TODO build iterator for:
                    # with open(cache_file, "r") as fh:
                    #   base64.encodestring(fh.read()).splitlines()
                    cmd_cat = ["/bin/cat", cache_file]
                    proc_cat = subprocess.Popen(cmd_cat, shell=False, stdout=subprocess.PIPE)
                    cmd_base64 = ["/usr/bin/base64","-"]
                    proc_base64 = subprocess.Popen(cmd_base64, shell=False, stdin=proc_cat.stdout, stdout=subprocess.PIPE)
                    for line in iter(proc_base64.stdout):
                        yield line
            finally:
                flo.unlock()
        else:
            self.lock.acquire()
            flo = flealock.flealock(cache_file)
            if flo.lock(wait=-1):
                try:
                    cmd_archive = ["/usr/bin/git", "--git-dir=%s" % self.full_path, "archive", "--format", "tar", commit]
                    if filelist:
                        cmd_archive += filelist
                    if self.log: self.log.debug("streaming using command: %s" % cmd_archive)
                    proc_archive = subprocess.Popen(cmd_archive, shell=False, stdout=subprocess.PIPE)
                    cmd_gzip = ["/bin/gzip","-c"]
                    proc_gzip = subprocess.Popen(cmd_gzip,shell=False, stdin=proc_archive.stdout, stdout=subprocess.PIPE)
                    cmd_tee = ["/usr/bin/tee", cache_file]
                    proc_tee = subprocess.Popen(cmd_tee, shell=False, stdin=proc_gzip.stdout, stdout=subprocess.PIPE)
                    cmd_base64 = ["/usr/bin/base64","-"]
                    proc_base64 = subprocess.Popen(cmd_base64, shell=False, stdin=proc_tee.stdout, stdout=subprocess.PIPE)
                    for line in iter(proc_base64.stdout):
                        yield line
                finally:
                    self.lock.release()
                flo.unlock()

