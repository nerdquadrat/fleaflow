#!/usr/bin/python

# author : Sven Wagner http://www.nerdquadrat.de
# license: GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

import yaml
import os
import sys
import pwd
import subprocess
import time
import flealock
import requests
import json
import hashlib
import urllib
import zlib
import base64
from cStringIO import StringIO
import tarfile

if os.path.exists("fleaxtract-dev.yaml"):
    CONF_FILE = "fleaxtract-dev.yaml"
else:
    CONF_FILE = "fleaxtract.yaml"

F_BSIZE = 0
F_BAVAIL = 4
COMMIT_LENGTH = 40

FS_MIN_FREE_PC = 10
FS_MIN_FREE_MB = 5

def recursive_rm_f(path):
    if os.path.exists(path):
        os.system("/bin/rm -rf '%s'" % path)

class FleaDir(object):
    def __init__(self, name, log=None):
        self.name = name
        self.log = log
        self.conf = self.read_conf()
        self.deployed_files = {}
        self.additional_files = {}
        self.errors = []
        self.fixes = []
        self.symlinks = {}
        self.md5_tables = {}

    def read_conf(self):
        with open(CONF_FILE, "r") as fh:
            return yaml.load(fh.read())["fleadirs"][self.name]

    def check_destination(self):
        if not os.path.isdir(self.conf["destination"]):
            if self.log: self.log.debug("FleaDir destination does not exist: %s" % self.conf["destination"])
            return False
        stat = os.stat(self.conf["destination"])
        if self.conf["dest-owner"] != pwd.getpwuid(stat.st_uid).pw_name:
            if self.log: self.log.debug("FleaDir bad owner of %s: %s, should be %s" % (self.conf["destination"], pwd.getpwuid(stat.st_uid).pw_name), self.conf["dest-owner"])
            return False
        if self.conf["dest-group"] != pwd.getpwuid(stat.st_gid).pw_name:
            if self.log: self.log.debug("FleaDir bad group of %s: %s, should be %s" % (self.conf["destination"], pwd.getpwuid(stat.st_uid).pw_name), self.conf["dest-group"])
            return False

        if self.conf["dest-dirmode"] != str(oct(stat.st_mode)[-4:]):
            if self.log: self.log.debug("FleaDir bad mode of %s: '%s', should be '%s'" % (self.conf["destination"], str(oct(stat.st_mode)[-4:]), self.conf["dest-dirmode"]))
            return False
        
        return True

    def check_mode(self, path, mode):
        real_mode = str(oct(os.stat(path).st_mode)[-4:])
        if real_mode == mode:
            return True
        return False

    def check_owner(self, path, owner):
#        if self.log: self.log.debug("check_owner %s to be %s" % (path, owner))
        stat = os.stat(path) # TODO call only once for each file
        real_owner = pwd.getpwuid(stat.st_uid).pw_name
        if real_owner == owner:
            return True
        return False

    def check_group(self, path, group):
        stat = os.stat(path) # TODO call only once for each file
        real_group = pwd.getpwuid(stat.st_gid).pw_name
        if real_group == group:
            return True
        return False

    def check_symlink(self, path, dest, commit=None):
        real_dest = os.readlink(path)
        if real_dest == dest:
            return True
        return False

    def check_md5(self, path, md5):
        if self.get_md5(path) == md5:
            return True
        return False

    def fix_mode(self, path, mode, commit=None):
        old_mode = str(oct(os.stat(path).st_mode)[-4:]) # TODO call stat only once for each file
        if old_mode == mode:
            return True
        os.chmod(path, int(mode,8)) # try/except ?
        new_mode = str(oct(os.stat(path).st_mode)[-4:]) # TODO if we want to call stat only once per file we now need to update the dict
        if new_mode == mode:
            return (old_mode, new_mode)
        return False

    def fix_owner(self, path, owner, commit=None):
        stat = os.stat(path) # TODO call only once for each file
        old_owner = pwd.getpwuid(stat.st_uid).pw_name
        if old_owner == owner:
            return True
        os.chown(path, pwd.getpwnam(owner).pw_uid, -1) # try/except ?
        new_owner = pwd.getpwuid(os.stat(path).st_uid).pw_name # TODO if we want to call stat only once per file we now need to update the dict
        if new_owner == owner:
            return (old_owner, new_owner)
        return False

    def fix_group(self, path, group, commit=None):
        stat = os.stat(path) # TODO call only once for each file
        old_group = pwd.getpwuid(stat.st_gid).pw_name
        if old_group == group:
            return True
        os.chown(path, -1, pwd.getpwnam(group).pw_gid) # try/except ?
        new_group = pwd.getpwuid(os.stat(path).st_gid).pw_name # TODO if we want to call stat only once per file we now need to update the dict
        if new_group == group:
            return (old_group, new_group)
        return False

    def fix_symlink(self, path, dest, commit=None):
        old_dest = os.readlink(path)
        if old_dest == dest:
            return True
        os.unlink(path) # TODO try/except ?
        os.symlink(dest, path) # TODO try/except ?
        new_dest = os.readlink(path)
        if new_dest == dest:
            return (old_dest, new_dest)
        return False

    def fix_md5(self, path, md5, commit=None):
        if self.check_md5(path, md5):
            return True

        rel_path = path[len(self.conf["destination"])+1:]
        if rel_path.startswith("extract."):
            rel_path = rel_path[9:]
        rel_path = rel_path[COMMIT_LENGTH+1:]

        if self.log: self.log.debug("fixing md5 of %s by downloading %s" % (path, rel_path))
        obj = self.fetchInstallFile(commit, rel_path)
        if self.log: self.log.debug("extracting file to %s" % path)
        with open(path, "wb") as fh:
            fh.write(obj.read())

        if not self.check_md5(path, md5):
            return False
        return True

    def fetchInstallFile(self, commit, rel_path):
        # re-fetch single file and save it to disk, to be called from fix_md5
        for url_template in self.conf["sources"]:
            url = url_template.replace("{REPO}", self.conf["repo"]).replace("{ACTION}","get_zipped_files").replace("{COMMIT}", commit)
            url += "&filelist=%s" % urllib.quote(rel_path, safe="")
            nul = open("/dev/null", "w")
            if self.log: self.log.debug(url)
            r = requests.get(url)
            if r.status_code != 200:
                continue
            b64 = base64.b64decode(r.content)
            tarcontent = zlib.decompress(b64, 16+zlib.MAX_WBITS)
            del b64
            tar = tarfile.open(mode= "r", fileobj=StringIO(tarcontent))
            del tarcontent
            for item in tar.getnames():
                if item == rel_path:
                    if self.log: self.log.debug("got file %s from tar" % item)
                    return tar.extractfile(item)
        return None

    def get_md5(self, path):
        h_md5 = hashlib.md5()
        with open(path, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                h_md5.update(chunk)
        return h_md5.hexdigest()

    def check_x(self, typ, path, value):
        if typ == "owner":
            return self.check_owner(path, value)
        if typ == "group":
            return self.check_group(path, value)
        if typ == "mode":
            return self.check_mode(path, value)
        if typ == "symlink":
            return self.check_symlink(path, value)
        if typ == "md5":
            return self.check_md5(path, value)

    def fix_x(self, typ, path, value, commit=None):
        if typ == "owner":
            return self.fix_owner(path, value)
        if typ == "group":
            return self.fix_group(path, value)
        if typ == "mode":
            return self.fix_mode(path, value)
        if typ == "symlink":
            return self.fix_symlink(path, value)
        if typ == "md5":
            return self.fix_md5(path, value, commit=commit)

    def check_permissions(self, path, isdir, apply_config, commit=None):
        rel_path = path[len(self.conf["destination"])+1:]
        if rel_path.startswith("extract."):
            rel_path = rel_path[COMMIT_LENGTH+9:]
        else:
            rel_path = rel_path[COMMIT_LENGTH+1:]
            
        perms = {}
        if rel_path in self.symlinks:
            True
        for typ in ["mode", "group", "owner", "md5"]:
            if "%s-list" % typ in self.conf and rel_path in self.conf["%s-list" % typ]:
                perms[typ] = self.conf["%s-list" % typ][rel_path]
            else:
                if typ == "mode":
                    if isdir:
                        perms[typ] = self.conf["dest-dirmode"]
                    else:
                        perms[typ] = self.conf["dest-filemode"]
                elif not isdir and commit and typ == "md5" and not os.path.islink(path):
                        perms[typ] = self.md5_tables[commit][rel_path]
                else:
                    perms[typ] = self.conf["dest-%s" % typ]

        for typ in perms:
            if not self.check_x(typ, path,  perms[typ]):
                if apply_config:
                    if typ == "md5":
                        result = self.fix_x(typ, path, perms[typ], commit=commit)
                    else:
                        result = self.fix_x(typ, path, perms[typ])
                    if result:
                        self.fixes.append({path: result})
                    else:
                        self.errors.append({path: result})
                else:
                    self.errors.append({path: [typ, perms[typ]]})

    def recursive_check_permissions(self, directory, commit, apply_config, check_md5_sum):
        if commit not in self.md5_tables:
            self.md5_tables[commit] = {}
            self.read_md5_file(commit)
 
        for (dirpath, dirnames, filenames) in os.walk(directory):
            for f in filenames:
                fname = os.path.join(dirpath, f)
                if fname not in self.deployed_files:
                    self.additional_files[fname] = True

                self.check_permissions(fname, False, apply_config, commit=commit)

            for d in dirnames:
                cdir = os.path.join(dirpath, d)
                self.check_permissions(cdir, True, apply_config)
                self.recursive_check_permissions(cdir, commit, apply_config, check_md5_sum)

    def read_md5_file(self, commit):
        md5_file = "%s/.%s.md5" % (self.conf["destination"], commit)
        fname_start_char = 36 + len(self.conf["destination"]) + COMMIT_LENGTH
        with open(md5_file, "r") as fh:
            for line in fh:
                self.md5_tables[commit][line[fname_start_char:-1]] = line[:32]

    def check_commit(self, commit, check_md5_sum=False, apply_config=False, check_lockfile_only=False, disable_lockfile_check=False):

        lockfile = "%s/.%s.lock" % (self.conf["destination"], commit)
        md5_file = "%s/.%s.md5" % (self.conf["destination"], commit)
        symlink_file = "%s/.%s.symlinks" % (self.conf["destination"], commit)

        if not disable_lockfile_check:
            if not os.path.exists(lockfile):
                if self.log: self.log.debug("lockfile not present: %s" % lockfile)
                return False
        
            mode = str(oct(os.stat(lockfile).st_mode)[-4:])
            if mode != "0770":
                if self.log: self.log.debug("lockfile bad permissions: %s, expecting 0770" % mode)
                return False

            if check_lockfile_only:
                return True

        if not os.path.exists(md5_file):
            if self.log: self.log.debug("md5 file not present: %s" % md5_file)
            return False

        if not os.path.exists(md5_file):
            if self.log: self.log.debug("symlink file not present: %s" % symlink_file)
            return False

        with open(md5_file, "r") as fh:
            for line in fh:
                if line.startswith("#"):
                    continue
                f = line[34:].strip("\n")
                self.deployed_files[f] = True

        with open(symlink_file, "r") as fh:
            for line in fh:
                #`/tmp/dest_tar_extract.d/2a0c02d5fbfe0907ce4b047262f5f59d5b7cee53/linktest' -> `/nonexistent'
                linkfile = line[1:line.find("'")]
                dest = line[line.find("`",2):-1] # TODO we assume no '`' and "'" quotes in the values ... maybe better use regex
                self.symlinks[linkfile] = dest
                self.deployed_files[linkfile] = True
                if not os.path.islink(linkfile):
                    self.errors.append({linkfile: "missing"})
                if apply_config:
                    result = fix_symlink(linkfile, dest)
                    if result is not True:
                        self.fixes.append({linkfile: result})
                else:
                    if not check_symlink(linkfile, dest):
                        self.errors.append({linkfile: ("bad link", "should be %s" % os.readlink(linkfile))})
 
        # check file permissions and find additional files:
        if "external-files" in self.conf:
            for f in self.conf["external-files"]:
                self.deployed_files[f] =  True
                if not os.path.exists(os.path.join(self.conf["destination"], f)):
                    self.errors.append({f: "missing"})

        self.recursive_check_permissions(os.path.join(self.conf["destination"], commit), commit, apply_config, check_md5_sum)
        if self.additional_files:
            if self.log: self.log.debug("additional files found: %s" % self.additional_files)
            return False
        if self.errors:
            if self.log: self.log.debug("files with errors found: %s" % self.errors)
            return False
            
        return True

    def cleanup_unused(self):
        for (dirpath, dirnames, filenames) in os.walk(self.conf["destination"]):
            for cdir in dirnames:
                # check that cdir looks like a commit id
                if len(cdir) != COMMIT_LENGTH and len(cdir.replace("extract.", "")) != COMMIT_LENGTH:
                    # log and ignore
                    continue

                check_dir = os.path.join(dirpath, cdir)
                lockfile = os.path.join(dirpath, ".%s.lock" % cdir)
                md5_file = os.path.join(dirpath, ".%s.md5" % cdir)
                symlink_file = os.path.join(dirpath, ".%s.symlinks" % cdir)

                if "extract." in cdir:
                    last_used = os.stat(lockfile).st_mtime
                    if last_used > int(time.time()) - self.conf["dest-cleanup-unused-seconds"]:
                        print("recent extract dir: %s, leaving as is" % cdir)
                        continue
                    else:
                        print("aged extract dir: %s, removing" % cdir)
                        recursive_rm_f(check_dir)
                        os.unlink(lockfile)
                        os.unlink(md5_file)
                        os.unlink(symlink_file)
                        continue
                
                if not os.path.exists(lockfile):
                    print("remove %s, no lockfile exists" % check_dir)
                    recursive_rm_f(check_dir)
                    os.unlink(lockfile)
                    os.unlink(md5_file)
                    os.unlink(symlink_file)
                else:
                    last_used = os.stat(lockfile).st_mtime
                    if last_used > int(time.time()) - self.conf["dest-cleanup-unused-seconds"]:
                        print("in use: %s, leaving as is" % lockfile)
                        continue
                    
                    l = flealock.flealock(lockfile)
                    if l.spider(shared=None) > 0:
                        print("lockfile was locked, resetting modtime, leaving as is" % lockfile)
                        os.utime(lockfile, (last_used, last_used))
                        continue
                    print("remove %s, lockfile is not used and not updated within %i seconds" % (check_dir, self.conf["dest-cleanup-unused-seconds"]))
                    recursive_rm_f(check_dir)
                    os.unlink(lockfile)
                    os.unlink(md5_file)
                    os.unlink(symlink_file)

class FleaXtractor(object):
    def __init__(self, name, commit, log=None):
        self.name = name
        self.commit = commit
        self.source = None
        self.size = None
        self.md5 = None
        self.log = log
        self.fleadir = FleaDir(self.name, log=log)
        dest = self.fleadir.conf["destination"]
        self.dest_tar_extract = os.path.join(dest, "extract.%s" % self.commit)
        self.dest_tar_moveto = os.path.join(dest, self.commit)
        self.dest_tar_moveto_md5 = "%s/.%s.md5" % (dest, self.commit)
        self.dest_tar_moveto_symlinks = "%s/.%s.symlinks" % (dest, self.commit)
        self.dest_tar_moveto_lock = "%s/.%s.lock" % (dest, self.commit)
        self.dest_tar_extract_download_lock = "%s/extract.%s.dl_lock" % (dest, self.commit)

    def install_commit(self, check_md5_sum=False, apply_config=False, check_lockfile_only=True, disable_lockfile_check=True):
        if self.fleadir.check_commit(self.commit, check_md5_sum=check_md5_sum, apply_config=apply_config, check_lockfile_only=check_lockfile_only, disable_lockfile_check=disable_lockfile_check):
            if self.log: self.log.debug("FleaDir check_commit successful, touching lockfile")
            os.utime(self.dest_tar_moveto_lock, None)
            return True

        if self.log: self.log.debug("check_commit failed, doing next steps")
        
        if not self.fleadir.check_destination():
            if self.log: self.log.debug("FleaDir check_destination FAILED")
            return False

        if not self.spider():
            if self.log: self.log.debug("FleaXtractor spider failed, but thats ok")

        if not self.get_commit_info():
            if self.log: self.log.debug("FleaXtractor get_commit_info FAILED")
            return False

        fs_stat = os.statvfs(self.fleadir.conf["destination"])
        available_bytes = fs_stat[F_BSIZE] * fs_stat[F_BAVAIL]
        if self.size + (available_bytes / 100) * FS_MIN_FREE_PC > available_bytes:
            if self.log: self.log.debug("FleaXtractor FS_MIN_FREE_PC to low")
            return False
        if self.size + FS_MIN_FREE_MB * 1024 * 1024 > available_bytes:
            if self.log: self.log.debug("FleaXtractor FS_MIN_FREE_MB to low")
            return False


        if not self.create_download_dir():
            if self.log: self.log.debug("FleaXtractor install_commit create_download_dir failed")
            return False

        if not self.fetchInstallArchive():
            if self.log: self.log.debug("FleaXtractor fetchInstallArchive failed")
            recursive_rm_f(self.dest_tar_extract)
            recursive_rm_f(self.dest_tar_moveto)
            self.dl_lock.unlock()
            return False
        self.dl_lock.unlock()
        os.unlink(self.dest_tar_extract_download_lock)
        self.dl_lock = None
        return True

    def create_download_dir(self):
        if os.path.exists(self.dest_tar_extract):
            if self.log: self.log.debug("FleaXtractor create_download_dir failed (path exists)")
            return False
        self.dl_lock = flealock.flealock(self.dest_tar_extract_download_lock)
        if not self.dl_lock.lock():
            if self.log: self.log.debug("FleaXtractor create_download_dir failed (cannot get lock)")
            self.dl_lock.unlock()
            return False
        try:
            os.mkdir(self.dest_tar_extract, 0700)
            return True
        except:
            if self.log: self.log.debug("FleaXtractor create_download_dir failed (os.makedir failed)")
            self.dl_lock.unlock()
            return False

    def spider(self):
        for url_template in self.fleadir.conf["sources"]:
            url = url_template.replace("{REPO}", self.fleadir.conf["repo"]).replace("{ACTION}","spider_archive_file").replace("{COMMIT}", self.commit)
            try:
                r = requests.get(url)
                if r.status_code != 200:
                    continue
            except:
                    continue
            self.sources_list = [url_template]
            return True
        self.sources_list = self.fleadir.conf["sources"]
        return False

    def get_commit_info(self):
        for url_template in self.sources_list:
            url = url_template.replace("{REPO}", self.fleadir.conf["repo"]).replace("{ACTION}", "get_commit_info").replace("{COMMIT}", self.commit)
            if self.log: self.log.debug("FleaXtractor get_commit_info: using url %s" % url)

            try:
                r = requests.get(url)
                if r.status_code != 200:
                    if self.log: self.log.debug("FleaXtractor get_commit_info: requests.get failed")
                    continue
            except:
                continue

            try:
                d = json.loads(r.text)
                self.md5 = d["md5"]
                self.size = d["bytes"]
                return True
            except:
                if self.log: self.log.debug("FleaXtractor get_commit_info: json.loads failed, trying next server if any")
                continue
        if self.log: self.log.debug("FleaXtractor get_commit_info: failed for all servers, thats bad")
        return False

           

    def fetchInstallArchive(self):
        # TODO use python base64, gzip, tar and md5sum and set file permissions on the fly

        for url_template in self.sources_list:
            url = url_template.replace("{REPO}", self.fleadir.conf["repo"]).replace("{ACTION}","get_zipped_archive").replace("{COMMIT}", self.commit)

            nul = open("/dev/null", "w")
            cmd_base64 = ["/usr/bin/base64", "-d"]
            proc_base64 = subprocess.Popen(cmd_base64, shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

            cmd_gzip = ["/bin/gzip","-d", "-c"]
            proc_gzip = subprocess.Popen(cmd_gzip,shell=False, stdin=proc_base64.stdout, stdout=subprocess.PIPE)

            cmd_tee = "/bin/bash -c '/usr/bin/tee /tmp/foo >(/bin/tar --no-same-owner --no-same-permissions -C '%s' -xf - ) >(/usr/bin/md5sum 1>&2) | wc -c'" % self.dest_tar_extract
            proc_tee = subprocess.Popen(cmd_tee, stdin=proc_gzip.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)

            if float(sys.version[:3]) < 3.6: 
                # no stream processing in old requests modules, maybe lower versions are ok, but 2.7.3 is too low and 3.6 works
                # TODO add stream handling for newer python version
                r = requests.get(url)
                if r.status_code == 200:
                    proc_base64.stdin.write(r.text)
                    proc_base64.stdin.flush()
                    proc_base64.stdin.close()
                    proc_base64.wait()
                    proc_gzip.wait()
                    proc_tee.wait()
                    output, err = proc_tee.communicate()
                    if proc_base64.returncode != 0 or proc_gzip.returncode != 0 or proc_tee.returncode != 0:
                        nul.close()
                        if self.log: self.log.debug("FleaXtractor fetchInstallArchive: bad returncodes, trying next source if any")
                        continue

                    dl_size = int(output)
                    dl_md5 = err[:32]
                    if dl_size == self.size and dl_md5 == self.md5:
                        os.rename(self.dest_tar_extract, self.dest_tar_moveto)

                        os.system("find '%s' -type f -exec md5sum '{}' \\; | sort > '%s'" % (self.dest_tar_moveto, self.dest_tar_moveto_md5))
                        os.system("find '%s' -type l -exec stat -c %%N '{}' \\; | sort > '%s'" % (self.dest_tar_moveto, self.dest_tar_moveto_symlinks))
                        self.fleadir.recursive_check_permissions(self.dest_tar_moveto, self.commit, True, False)
                        if self.fleadir.errors:
                            # TODO cleanup
                            nul.close()
                            if self.log: self.log.debug("FleaXtractor fetchInstallArchive: errors while fleadir.recursive_check_permissions")
                            continue

                        # TODO move FleaDir.check|fix_*() from methods into functions
                        if not self.fleadir.fix_mode(self.dest_tar_moveto, self.fleadir.conf["dest-dirmode"]):
                            if self.log: self.log.debug("FleaXtractor fetchInstallArchive: error while setting mode for %s" % self.dest_tar_moveto)
                            continue
                        # existance of lockfile defines ready-to-use, created last
                        if self.log: self.log.debug("FleaXtractor fetchInstallArchive: finishing: creating lockfile %s" % self.dest_tar_moveto_lock)

                        open(self.dest_tar_moveto_lock,"w").close()
                        if not self.fleadir.fix_group(self.dest_tar_moveto_lock, self.fleadir.conf["dest-group"]):
                            if self.log: self.log.debug("FleaXtractor fetchInstallArchive: error while setting group for %s" % self.dest_tar_moveto_lock)
                            continue
                        if not self.fleadir.fix_mode(self.dest_tar_moveto_lock, "0770"):
                            os.unlink(self.dest_tar_moveto_lock)
                            if self.log: self.log.debug("FleaXtractor fetchInstallArchive: error while setting mode for %s" % self.dest_tar_moveto_lock)
                            continue

                        nul.close()
                        return True

                    else:
                        nul.close()
                        recursive_rm_f(self.dest_tar_extract)

                        if self.log: self.log.debug("FleaXtractor fetchInstallArchive: dl_size(%i/%i) or md5(%s/%s) mismatch, trying next source if any: [%s]" % (dl_size,self.size , dl_md5, self.md5, output))

#            else: # version < 3.6
#                r = requests.get(url, stream=True)
#                for chunk in r.iter_content(chunk_size=512):
#                    if chunk:
#                        proc_base64.stdin.write(chunk)
#                    else:
#                        break
#                if r.status_code == 200:
#                    proc_base64.wait()
#                    proc_gzip.wait()
#                    proc_tee.wait()
#
#                    output, err = proc_tee.communicate()
#                    dl_size = int(output)
#                    dl_md5 = err[:32]
#
