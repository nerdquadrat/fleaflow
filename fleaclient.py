#!/usr/bin/python

# author : Sven Wagner http://www.nerdquadrat.de
# license: GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html

import sys
import os
sys.path.append("lib")
import fleaxtract
import logging
import logging.handlers

log = logging.getLogger('fleaclient')
hdlr = logging.handlers.WatchedFileHandler('log/fleaclient.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
log.addHandler(hdlr)
log.setLevel(logging.DEBUG)
log.info("INIT [%i] with args %s" % (os.getpid(), sys.argv))


fx = fleaxtract.FleaXtractor(sys.argv[1], sys.argv[2], log=log)
if fx.install_commit():
    print("success")
    sys.exit(0)
else:
    print("fail")
    sys.exit(1)
    
log.close()
